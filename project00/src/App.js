import React, { Component } from "react";

import Persone from "./Components/Personne/Persone";
import Horloge from "./Container/Horloge/Horloge";
import AgePersonne from "./Components/Personne/AgePersonne/AgePersonne";

import "./App.css";

class App extends Component {
  state = {
    personnes: [
      { id: 1, name: "Mathieur", age: 31, sexe: true },
      { id: 2, name: "Tya", age: 25, sexe: false },
      { id: 3, name: "Milo", age: 43, sexe: true },
    ],
  };

  handleBirthday = (id) => {
    const numberCaseTabPersone = this.state.personnes.findIndex((element) => {
      return element.id === id;
    });

    const newPersonne = { ...this.state.personnes[numberCaseTabPersone] }; // génére une copire de la personne sur laquelle on a cliqué
    newPersonne.age++; // augmente l'age de la personne copiée
    const newTab = [...this.state.personnes]; // on duplique le tableau de personnes
    newTab[numberCaseTabPersone] = newPersonne; // on rempplace la personne ç l'indice du tableau sur lequel on a cliqué par la nouvel personne qu'on à crée
    this.setState({ personnes: newTab }); // on remplace dans le state le tableau de personne par le nouveau tableau
  };

  render() {
    return (
      <>
        <button onClick={this.handleBirthday}>Anniversaire</button>
        <Horloge />
        {this.state.personnes.map((personne) => {
          return (
            <Persone
              key={personne.id}
              {...personne}
              click={() => this.handleBirthday(personne.id)}
            >
              <AgePersonne age={personne.age} />
            </Persone>
          );
        })}

        {/* <Persone
          {...this.state.personnes[0]}
          click={this.handleBirthday.bind(this, 0)}
        >
          <AgePersonne age={this.state.personnes[0].age} />
        </Persone>
        <Persone
          {...this.state.personnes[1]}
          click={() => this.handleBirthday(1)}
        >
          <AgePersonne age={this.state.personnes[1].age} />
        </Persone>
        <Persone
          {...this.state.personnes[2]}
          click={() => this.handleBirthday(2)}
        >
          <AgePersonne age={this.state.personnes[2].age} />
        </Persone> */}
      </>
    );
  }
}

export default App;
