import React, { Component } from "react";

import classes from "./Horloge.module.css";

class Horloge extends Component {
  state = {
    date: new Date(),
    compteur: 1,
  };

  tick = () => {
    this.setState((oldStates, props) => {
      return {
        date: new Date(),
        compteur: oldStates.compteur + 1,
      };
    });
  };

  componentDidMount() {
    this.timerId = setInterval(() => {
      this.tick();
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerId);
  }

  render() {
    return (
      <>
        <h2 className={classes.myTitle}>
          Horloge : {this.state.date.toLocaleTimeString()}
        </h2>
        <div>Compteur : {this.state.compteur}</div>
      </>
    );
  }
}

export default Horloge;
