import React, { Component } from "react";
import AgePersonne from "./AgePersonne/AgePersonne";

import classes from "./Personne.module.css";

class Persone extends Component {
  render() {
    let myStyle = {
      backgroundColor: "crimson",
      borderRadius: 15,
      color: "white",
    };
    myStyle.fontSize = "19px";

    if (this.props.sexe) {
      myStyle.backgroundColor = "blue";
    } else {
      myStyle.backgroundColor = "crimson";
    }

    return (
      <>
        <h1 className={classes.myTitle}>{this.props.name}</h1>
        {this.props.children}
        <div style={myStyle}>Sexe : {this.props.sexe ? "Homme" : "Femme"}</div>
        <button onClick={this.props.click}>Anniversaire</button>
      </>
    );
  }
}

export default Persone;
