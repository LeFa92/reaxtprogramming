import React from "react";

const AgePersonne = (props) => {
  let now = new Date();
  let year = now.getFullYear();
  return (
    <div>
      Age : {props.age} - ({year - props.age})
    </div>
  );
};

export default AgePersonne;
